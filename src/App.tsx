import React, { useState } from "react";
import { io } from "socket.io-client";
import AuthLayout from "./Layout/AuthLayout";
import ChatScreen from "./Screens/ChatScreen";
import { SERVER_API } from "./Constants";
import SignIn from "./Screens/SignIn";

const socket = io(`${SERVER_API}/chat`);

const App: React.FC = () => {
  const [name, setName] = useState<string>("");
  const [room, setRoom] = useState<string>("");
  const [hasHistory, setHasHisttory] = useState<boolean>(false);
  const [showChat, setShowChat] = useState<boolean>(false);

  const joinRoom = () => {
    if (name !== "" && room !== "") {
      socket.emit("joinRoom", room);
      setShowChat(true);
    }
  };

  const changeRoom = (roomID: string) => {
    setRoom(roomID);
  };

  const fetchHistory = (trigger: boolean) => {
    setHasHisttory(trigger);
  };
  return (
    <div className="flex min-w-screen min-h-screen bg-slate-50 h-1">
      {!showChat ? (
        <SignIn setName={setName} setRoom={setRoom} joinRoom={joinRoom} />
      ) : (
        <AuthLayout
          user={name}
          room={room}
          hasHistory={hasHistory}
          changeRoom={changeRoom}
        >
          <ChatScreen
            room={room}
            socket={socket}
            username={name}
            fetchHistory={fetchHistory}
          />
        </AuthLayout>
      )}
    </div>
  );
};

export default App;
