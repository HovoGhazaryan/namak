import { SERVER_API } from "../../Constants";

export const getMessageHistory = async (sender: string) => {
  return await fetch(`${SERVER_API}/chat/message-history?sender=${sender}`)
    .then((response) => {
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      return response.json();
    })
};

export const getRoomMessages = async (room: string) => {
  return await fetch(`${SERVER_API}/chat/messages?room=${room}`)
    .then(response => {
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      return response.json();
    })
}