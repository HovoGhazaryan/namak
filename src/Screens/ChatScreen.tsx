import React, { useEffect, useState } from "react";
import ScrollToBottom from "react-scroll-to-bottom";
import { getRoomMessages } from "../Services/API/messages";
import { Message } from "../Types";

interface ChatScreenProps {
  socket: any;
  username: string;
  room: string;
  fetchHistory: (trigger: boolean) => void;
}

const ChatScreen: React.FC<ChatScreenProps> = ({
  room,
  socket,
  username,
  fetchHistory,
}) => {
  const [currentMessage, setCurrentMessage] = useState("");
  const [messageList, setMessageList] = useState<Message[]>([]);

  const sendMessage = async () => {
    if (currentMessage !== "") {
      const messageData = {
        sender: username,
        room: room,
        message: currentMessage,
        time:
          new Date(Date.now()).getHours() +
          ":" +
          new Date(Date.now()).getMinutes(),
      };

      await socket.emit("chatToServer", messageData);
      setCurrentMessage("");
    }
  };

  useEffect(() => {
    socket.on("chatToClient", (data: Message) => {
      setMessageList((list) => [...list, data]);
    });

    return () => {
      socket.off("chatToClient");
    };
  }, [socket]);

  useEffect(() => {
    getRoomMessages(room)
      .then((res) => setMessageList(res.messages))
      .catch((error) => {
        console.error("Fetch error:", error);
      });
  }, [room]);

  useEffect(() => {
    if (messageList.length === 1) {
      fetchHistory(true);
    }
  });
  return (
    <div className="h-full w-full mx-auto my-auto bg-white rounded-xl shadow-lg">
      <div className="lg:h-[calc(100%-40px)] border  border-black bg-white relative">
        <ScrollToBottom className="h-full w-full overflow-y-hidden overflow-x-hidden snap-none">
          {messageList.map((messageContent, index) => {
            return (
              <div
                key={index}
                className={
                  username === messageContent.sender
                    ? "h-auto padding-3 px-5 flex mt-2 justify-end"
                    : "h-auto padding-3 px-5 flex mt-2 justify-start"
                }
              >
                <div>
                  <div className="flex">
                    <img
                      className="object-cover w-8 h-8 rounded-full"
                      src={
                        username === messageContent.sender
                          ? "https://clipart-library.com/images/8iEbMG55T.jpg"
                          : "https://wallpapers.com/images/hd/yao-ming-meme-face-526f5gwauekrvu0v.jpg"
                      }
                      alt=""
                    />
                    <div
                      className={
                        username === messageContent.sender
                          ? "h-auto w-auto h-min-40px w-max-120px bg-blue-600 rounded text-white flex align-center mx-1 p-1 break-words font-semibold justify-end"
                          : "h-auto w-auto h-min-40px w-max-120px bg-gray-600 rounded text-white flex align-center mx-1 p-1 break-words font-semibold justify-start"
                      }
                    >
                      <p>{messageContent.message}</p>
                    </div>
                  </div>
                  <div
                    className={
                      username === messageContent.sender
                        ? "h-auto padding-3 flex mt-2 justify-end mr-5"
                        : "h-auto padding-3 flex mt-2 justify-start ml-3"
                    }
                  >
                    <p>{messageContent.time}</p>
                    <p className="ml-2 text-bold">{messageContent.sender}</p>
                  </div>
                </div>
              </div>
            );
          })}
        </ScrollToBottom>
      </div>
      <div className="h-10 border border-black flex border-t-0">
        <input
          type="text"
          value={currentMessage}
          placeholder="Type a message"
          onChange={(event) => {
            setCurrentMessage(event.target.value);
          }}
          onKeyPress={(event) => {
            event.key === "Enter" && sendMessage();
          }}
          className="h-full flex basis-4/5 outline-none px-3"
        />

        <button
          onClick={sendMessage}
          className="h-full items-center flex basis-1/5 justify-center outline-none bg-indigo-600 text-white font-bold text-xl"
        >
          SEND {"->"}
        </button>
      </div>
    </div>
  );
};

export default ChatScreen;
