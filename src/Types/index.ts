export type Message = {
  sender: string;
  room: string;
  message: string;
  time?: string;
};