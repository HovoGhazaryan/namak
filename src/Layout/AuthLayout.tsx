import React, { useEffect, useState } from "react";
import { getMessageHistory } from "../Services/API/messages";
import { Message } from "../Types";
import AuthSidebar from "../Components/AuthSidebar";

interface AuthLayoutProps {
  user: string;
  room: string;
  changeRoom: (room: string) => void;
  children: any;
  hasHistory: boolean;
}

const AuthLayout: React.FC<AuthLayoutProps> = ({
  user,
  room,
  children,
  hasHistory,
  changeRoom,
}) => {
  const [messageHistory, setMessageHistory] = useState<Message[]>([]);

  useEffect(() => {
    getMessageHistory(user)
      .then((res: any) => setMessageHistory(res.messages))
      .catch((error: any) => {
        console.error("Fetch error:", error);
      });
  }, [room, user, hasHistory]);

  return (
    <div className="flex w-full">
      <AuthSidebar messageHistory={messageHistory} changeRoom={changeRoom} />
      {children}
    </div>
  );
};

export default AuthLayout;
